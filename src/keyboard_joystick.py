#!/usr/bin/env python

import sys

# ROS
import roslib
import rospy

# ROS msg
import std_msgs.msg
from sensor_msgs.msg import Joy

# QT libraries
from PySide import QtCore, QtGui

axesScale = 1.0
emg_btn_index = 1

class KeyMapping(object):
   l_up     = QtCore.Qt.Key.Key_W
   l_down   = QtCore.Qt.Key.Key_S
   l_left   = QtCore.Qt.Key.Key_A
   l_right  = QtCore.Qt.Key.Key_D
   r_up     = QtCore.Qt.Key.Key_I
   r_down   = QtCore.Qt.Key.Key_K
   r_left   = QtCore.Qt.Key.Key_J
   r_right  = QtCore.Qt.Key.Key_L
   btn_0    = QtCore.Qt.Key.Key_1
   btn_1    = QtCore.Qt.Key.Key_2
   btn_2    = QtCore.Qt.Key.Key_3
   btn_3    = QtCore.Qt.Key.Key_4
   btn_4    = QtCore.Qt.Key.Key_5
   btn_5    = QtCore.Qt.Key.Key_6
   btn_6    = QtCore.Qt.Key.Key_7
   btn_7    = QtCore.Qt.Key.Key_8
   btn_emg  = QtCore.Qt.Key.Key_Space

def getHeader():
   h = std_msgs.msg.Header()
   h.stamp = rospy.Time.now()
   return h

class KeyboardJoystickDisplay(QtGui.QMainWindow):
   '''
   Reading input from the keyboard and publishing to /joy topic
   ---------------------------
   Left Joystick:
    w
   asd

   Right Joystick:
    i
   jkl

   buttons [0...7]:
   1 2 3 4 5 6 7 8

   Emergency stop: space
          mapping: btn[1]
   ---------------------------
   '''
   def __init__(self):
      # construct parent class
      super(KeyboardJoystickDisplay, self).__init__()
      
      # construct QT main window
      self.setWindowTitle('Keyboard Joystick')

      # construct Joy msg
      self.joyMsg = Joy()
      self.joyMsg.axes    = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0]
      self.joyMsg.buttons = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]

      # init publisher
      self.pubJoy = rospy.Publisher('/joy', Joy)     

   def keyPressEvent(self, event):
      key = event.key()
      
      if key == KeyMapping.l_up:
         self.joyMsg.axes[1] += 1.0 * axesScale
      if key == KeyMapping.l_down:
         self.joyMsg.axes[1] += -1.0 * axesScale
      if key == KeyMapping.l_left:
         self.joyMsg.axes[0] += 1.0 * axesScale
      if key == KeyMapping.l_right:
         self.joyMsg.axes[0] += -1.0 * axesScale

      if key == KeyMapping.r_up:
         self.joyMsg.axes[3] += 1.0 * axesScale
      if key == KeyMapping.r_down:
         self.joyMsg.axes[3] += -1.0 * axesScale
      if key == KeyMapping.r_left:
         self.joyMsg.axes[2] += 1.0 * axesScale
      if key == KeyMapping.r_right:
         self.joyMsg.axes[2] += -1.0 * axesScale

      if key == KeyMapping.btn_0:
         self.joyMsg.buttons[0] = 1
      if key == KeyMapping.btn_1:
         self.joyMsg.buttons[1] = 1
      if key == KeyMapping.btn_2:
         self.joyMsg.buttons[2] = 1
      if key == KeyMapping.btn_3:
         self.joyMsg.buttons[3] = 1
      if key == KeyMapping.btn_4:
         self.joyMsg.buttons[4] = 1
      if key == KeyMapping.btn_5:
         self.joyMsg.buttons[5] = 1
      if key == KeyMapping.btn_6:
         self.joyMsg.buttons[6] = 1
      if key == KeyMapping.btn_7:
         self.joyMsg.buttons[7] = 1

      if key == KeyMapping.btn_emg:
         self.joyMsg.buttons[emg_btn_index] = 1

      self.joyMsg.header = getHeader()
      
      #pub joy msg
      self.pubJoy.publish(self.joyMsg)

   def keyReleaseEvent(self,event):
      key = event.key()
      
      if key == KeyMapping.l_up:
         self.joyMsg.axes[1] += -1.0 * axesScale
      if key == KeyMapping.l_down:
         self.joyMsg.axes[1] += 1.0 * axesScale
      if key == KeyMapping.l_left:
         self.joyMsg.axes[0] += -1.0 * axesScale
      if key == KeyMapping.l_right:
         self.joyMsg.axes[0] += 1.0 * axesScale

      if key == KeyMapping.r_up:
         self.joyMsg.axes[3] += -1.0 * axesScale
      if key == KeyMapping.r_down:
         self.joyMsg.axes[3] += 1.0 * axesScale
      if key == KeyMapping.r_left:
         self.joyMsg.axes[2] += -1.0 * axesScale
      if key == KeyMapping.r_right:
         self.joyMsg.axes[2] += 1.0 * axesScale

      if key == KeyMapping.btn_0:
         self.joyMsg.buttons[0] = 0
      if key == KeyMapping.btn_1:
         self.joyMsg.buttons[1] = 0
      if key == KeyMapping.btn_2:
         self.joyMsg.buttons[2] = 0
      if key == KeyMapping.btn_3:
         self.joyMsg.buttons[3] = 0
      if key == KeyMapping.btn_4:
         self.joyMsg.buttons[4] = 0
      if key == KeyMapping.btn_5:
         self.joyMsg.buttons[5] = 0
      if key == KeyMapping.btn_6:
         self.joyMsg.buttons[6] = 0
      if key == KeyMapping.btn_7:
         self.joyMsg.buttons[7] = 0

      if key == KeyMapping.btn_emg:
         self.joyMsg.buttons[emg_btn_index] = 0

      self.joyMsg.header = getHeader()

      #pub joy msg
      self.pubJoy.publish(self.joyMsg)


if __name__ == '__main__':
   rospy.init_node('keyboard_joystick')
   app = QtGui.QApplication(sys.argv)
   display = KeyboardJoystickDisplay()
   display.show()
   status = app.exec_()
   rospy.signal_shutdown('Shutdown...')
   sys.exit(status)
